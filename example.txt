Key : use Value : vbp
collectionVerb is: use in this line :
Information Provided By You : Activision collects Information from you when you: (1) open an Activision account with an Activision Property; (2) participate in Activision contests and promotions; (3) use Activision customer service; (4) download demos from Activision; (5) buy Activision services or products online; (6) use an Activision Property; (7) apply for job openings (8) use "send-a-friend" functionality (9) allow sharing through Social Media. 
 The ontology phrases are :media,
 
 
 Key : use Value : vb
collectionVerb is: use in this line :
Whenever you use an Activision Property we may: collect, process, and combine data such as your age, gender, interests, name, email address, gameplay, marketing preferences, customer service, and device-related information (like your IP address) contact you from time to time to request your opinion about Activision Properties, provide you with customer service, and other services you have requested use web beacons, cookies and similar technologies , or flash technology on Activision Properties or in e-mail messages to tailor content, marketing and improve and adjust user experience disclose Information to Activision group companies share Information with suppliers and partners in order to provide you with goods or services you have requested or online advertising selected by us. We do not share Information with third parties to use for their own direct marketing purposes without your agreement. collect, process and store Information on servers in the United States or other countries where Activision group companies or suppliers maintain facilities or business operations; in some countries, the data protection laws in these territories may not be as comprehensive as those in the European Union, but Activision takes reasonable measures to safeguard your privacy rights in accordance with this Privacy Policy 
 The ontology phrases are :time, country, technology, message, 
 
 Key : collect Value : vbz
collectionVerb is: collect in this line :
Activision collects certain personal information from you when you register with an Activision Property, such as your name, address, e-mail address, and phone number. 
 The ontology phrases are :
 
 Key : use Value : vbn
collectionVerb is: use in this line :
When Activision runs contests or promotions on Activision Properties it collects certain personal information from participants such as their name, address, e-mail address, phone number, and age. That information will be used in accordance with this Privacy Policy, unless otherwise stated in the contest terms or rules. 
 The ontology phrases are :state, 
 
 
 
