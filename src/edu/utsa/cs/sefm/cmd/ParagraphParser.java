package edu.utsa.cs.sefm.cmd;


import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.stanford.nlp.trees.Tree;
/**
 * Created by Mitra on 2/28/2016.
 */
public class ParagraphParser {
    public static List<String> ParagraphParser(String text) {
        Runtime rt = Runtime.getRuntime();
        final List<String> parseTreeList = new ArrayList<>();
        //ArrayList<Tree> treeList = new ArrayList<>();

        try {


            URL location = Lemmatizer.class.getProtectionDomain().getCodeSource().getLocation();
            System.out.println("Location of the lemmatizer class: "+location.toString());
            String loc = location.toString();
            System.out.println(loc);
            int index = loc.lastIndexOf("out/");
            System.out.println("NLP last index" + index);
            loc = loc.replaceAll(loc.substring(index, loc.length() ), "").concat("NLP").replaceAll("\\/", "\\\\");
            if (loc.startsWith("file:\\")){
                loc = loc.replace("file:\\", "");
            }
            //System.out.println("java -Xmx4g -cp "+loc+"\\stanford-corenlp-3.5.2.jar;"+loc+"\\stanford-corenlp-3.5.2-models.jar;"+loc+"\\utsa-parser.jar;"+loc+"; edu.utsa.cs.sefm.parser.NlpParser ");
            final Process pr = rt.exec("java -Xmx4g -cp "+loc+"\\utsa-corenlp-0.0.jar;"+loc+"\\utsa-corenlp-models-0.1.jar;"+loc+"\\utsa-parser-0.1.jar;"+loc+"; edu.utsa.cs.sefm.parser.NlpParser ");


            new Thread(new Runnable() {
                public void run() {
                    OutputStream outStream = pr.getOutputStream();
                    PrintWriter pWriter = new PrintWriter(outStream);
                    String currParagraph;
                    Scanner sc = new Scanner(text);
                    while (sc.hasNextLine()) {
                        currParagraph = sc.nextLine();
                        if (!currParagraph.isEmpty()) {
                            System.out.println("In here: " +currParagraph);
                            pWriter.println(currParagraph);
                        }
                    }
                    pWriter.flush();
                    pWriter.close();
                    BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                    String line = null;

                    try {
                        while ((line = input.readLine()) != null){
                            System.out.println("here: "+ line);
                            parseTreeList.add(line);
                            /*Tree t = Tree.valueOf(line);
                            if (t.label().value().equals("X"))
                                continue;
                            treeList.add(t);*/
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }).start();
            pr.waitFor();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return parseTreeList;
    }
}
