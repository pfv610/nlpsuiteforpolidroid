package edu.utsa.cs.sefm.cmd;

/**
 * Created by Mitra on 2/26/2016.
 */
import java.io.*;
import java.util.*;

import edu.utsa.cs.sefm.cmd.ParagraphProcessor;

/**
 * Created by Mitra on 2/8/2016.
 */
public class PolicyReader {

    public static List<String> ontologyPhrases = Collections.synchronizedList(new ArrayList<>());
    public static List<String> lemmaOntologyPhrases = Collections.synchronizedList(new LinkedList<>());
    //public static List<String> lemmatizedOntologyPhrases = Collections.synchronizedList(new ArrayList<>());

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length < 2)
            return;
        File ontologyFile = new File(args[1]);
        OntologyOWLAPI.loader(ontologyFile);
        ontologyPhrases = OntologyOWLAPI.ListOfOntologyPhrases(OntologyOWLAPI.ontology);
        //File[] policies = null;
        File directory = new File(args[0]);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                System.out.println(file.getName());
                String documentPlaintext = HTMLUtils.getText(file);
                // System.out.println("Content Of Policy: " +  file.getName()+ "\n"+ documentPlaintext);
                ParagraphProcessor paragraphProcessor = new ParagraphProcessor();
                for(String phrase: ontologyPhrases ){
                    System.out.println(phrase);
                }
                String sb = "";
                for(int i = 0; i<ontologyPhrases.size(); i++){
                    sb += ontologyPhrases.get(i);
                    sb += ". ";

                }
                System.out.println(sb);
                lemmaOntologyPhrases = Lemmatizer.lemmatizeRList(sb.toString());
                System.out.println("lemmatized ontology phrseas are:");
                for(String phrase: lemmaOntologyPhrases){
                  System.out.println(phrase);
                }
                paragraphProcessor.processParagraphs(documentPlaintext);
                paragraphProcessor.findPhrasesInOntology();

                System.out.println("lemmatized phrases in the ontology: ");
                System.out.println("number of ontology phrases: "+ ontologyPhrases.size());
                System.out.println("number of lemmatized ontology phrases: "+ lemmaOntologyPhrases.size());

                for (String phrase: ontologyPhrases){
                    System.out.println(phrase);
                }
                System.out.println("***********************Lemmatized**********************");
                for (String phrase: lemmaOntologyPhrases){
                    System.out.println(phrase);
                }
                System.out.println("Phrases in the ontology and privacy policy are:");
                for(String phraseInPolicy: ParagraphProcessor.phrasesInPolicy){
                    System.out.println(phraseInPolicy);
                }


                writePhrasesToFile (ParagraphProcessor.phrasesInPolicy, file.getName());
                ParagraphProcessor.phrasesInPolicy.clear();
            }
        }
    }


    public static void writePhrasesToFile(List<String> phrasesInPolicy, String fileName ){
        File file = new File("C://Users/Mitra/IdeaProjects/RunningUsingCMD/newFolderOut/"+ fileName.replaceAll(".html", ".txt") );

        // if file doesnt exists, then create it
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            for(String phrase: phrasesInPolicy) {
                // System.out.println(phrase);
                fw.write(phrase + "\n");
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

