package edu.utsa.cs.sefm.cmd;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.LinkedList;


/**
 * Created by Mitra on 11/14/2015.
 */


public class Lemmatizer {

    public static LinkedList<String> lemmatizeRList(String currline) {
       // System.out.println(currline);

        Runtime rt = Runtime.getRuntime();
        LinkedList<String> lemmas = new LinkedList<>();
       // final LinkedHashMap<String,String> lemmas = new LinkedHashMap<String, String>();
        try {
            final Process pr = rt.exec("java -cp NLP\\utsa-corenlp-0.0.jar;NLP\\utsa-corenlp-models-0.1.jar;NLP\\utsa-lemmatizer-0.1.jar;NLP; edu.utsa.cs.sefm.lemmatizer.Lemmatize ");
            OutputStream outStream = pr.getOutputStream();
            PrintWriter pWriter = new PrintWriter(outStream);
            pWriter.println(currline);
            pWriter.flush();
            pWriter.close();

            new Thread(new Runnable() {
                public void run() {

                    BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                    String line = null;
                    StringBuilder phrase = new StringBuilder();
                    try {
                        while ((line = input.readLine()) != null){
                            System.out.println("line " + line);
                            if (line.contains(".")){
                                if(!phrase.toString().isEmpty()) {
                                    lemmas.add(phrase.toString().substring(0,phrase.length()-1));
                                    phrase = new StringBuilder();
                                }
                                continue;
                            }
                            //System.out.println(line);
                            String[] parts = line.split(" = ");
                            //System.out.println(parts.length);
                            phrase.append(parts[0] + " ");
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }).start();
            pr.waitFor();
        } catch (Exception e1) {
            System.out.println("unable to ...");
            e1.printStackTrace();
        }
        return lemmas;
    }
    public static LinkedHashMap<String, String> lemmatizeRMap(String currline) {
        // System.out.println(currline);
        Runtime rt = Runtime.getRuntime();
        final LinkedHashMap<String,String> lemmas = new LinkedHashMap<String, String>();
        try {
            final Process pr = rt.exec("java -cp NLP\\stanford-corenlp-3.5.2.jar;NLP\\utsa-corenlp-models-0.1.jar;NLP\\utsa-lemmatizer-0.1.jar;NLP; edu.utsa.cs.sefm.lemmatizer.Lemmatize ");
            OutputStream outStream = pr.getOutputStream();
            PrintWriter pWriter = new PrintWriter(outStream);
            pWriter.println(currline);
            pWriter.flush();
            pWriter.close();

            new Thread(new Runnable() {
                public void run() {

                    BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                    String line = null;

                    try {
                        while ((line = input.readLine()) != null){
                            //System.out.println(line);
                            String[] parts = line.split(" = ");
                            //System.out.println(parts.length);
                            lemmas.put(parts[0], parts[1]);
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }).start();
            pr.waitFor();
        } catch (Exception e1) {
            System.out.println("unable to ...");
            e1.printStackTrace();
        }
        return lemmas;
    }
}




