package edu.utsa.cs.sefm.cmd;

/**
 * Created by Mitra on 2/26/2016.
 */

/**
 * Created by Mitra on 2/8/2016.
 */
import java.io.File;
import java.util.*;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public class OntologyOWLAPI {
    static OWLOntologyManager man;
    static OWLDataFactory fact;
    public static OWLOntology ontology;
    /**
     * loads the ontology from the specified file
     * @param ontologyFile
     */
    public static void loader(File ontologyFile){
        try {
            man = OWLManager.createOWLOntologyManager();
            fact = man.getOWLDataFactory();
            ontology = man.loadOntologyFromOntologyDocument(ontologyFile);
            System.out.println("Ontology was read successfully:"+ ontology.getAxiomCount());
        }catch (OWLException e) {
            e.printStackTrace();
        }
    }
    public static List<String> ListOfOntologyPhrases(OWLOntology ontology){
        List<String> allOntologyPhrases = new ArrayList<>();
        for (OWLClass cls : ontology.getClassesInSignature()){
            String phrase = cls.getIRI().toString().substring(cls.getIRI().toString().indexOf('#')+1).toLowerCase().replaceAll("_", " ");
            allOntologyPhrases.add(phrase);
        }
        return allOntologyPhrases;
    }



}
